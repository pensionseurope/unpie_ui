import * as types from '../../constants/actionTypes';
import {fectAndDispact} from '../ActionHelper';

export function case15nUpdateSlider(name, value, oldSlider) {
  
  const slider = Object.assign({}, oldSlider,
     {
       [name]: value
      });
  return {
    type: types.CASE15n_UPDATE_SLIDER,
    slider
  };
}

export function fetchDataSuccess(items,slider) {
  return {
      type: types.CASE15n_FETCH_DATA_SUCCESS,
      items,
      slider
  };
}

export function case15nUpdateGraph(slider) {
  const url = `https://api.unpie.eu/wrapper.case15n?x=${slider.pensionAge}&lambda=${slider.accidentalDeathRate}&m=${slider.modalDeathRate}&b=${slider.dispersionDeathRate}&r=${slider.rate}&sigma=${slider.vol}&inflation=0&B=${slider.pmt}&K=${slider.wealth}`;
  const func = fetchDataSuccess;
  const additionParams = [slider];
  return fectAndDispact(url,func,additionParams);
}