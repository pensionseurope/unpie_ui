import * as types from '../../constants/actionTypes';
import {fectAndDispact} from '../ActionHelper';

export function case6UpdateSlider(name, value, oldSlider) {
  const slider = Object.assign({}, oldSlider, {[name]: value});
  return {
    type: types.CASE6_UPDATE_SLIDER,
    slider
  };
}

export function case6SpendingRequiredFetchDataSuccess(items) {
  return {
      type: types.CASE6_SPENDING_REQUIRED_FETCH_DATA_SUCCESS,
      items
  };
}

export function case6SpendingWithoutInterestFetchDataSuccess(items) {
    return {
        type: types.CASE6_SPENDING_WITHOUTINTEREST_FETCH_DATA_SUCCESS,
        items
    };
}

export function case6UpdateGraph(slider) {
  
  const pv = `https://api.unpie.eu/pv.annuity?rate=${slider.rate}&nper=${slider.nper}&pmt=-${slider.pmt}`;
  const fv = `https://api.unpie.eu/fv?nper=${slider.nper}&pmt=-${slider.pmt}`;

  const funcPv = case6SpendingRequiredFetchDataSuccess;
  const funcFv = case6SpendingWithoutInterestFetchDataSuccess;

  return (dispatch) => {
    dispatch(fectAndDispact(pv,funcPv));
    dispatch(fectAndDispact(fv,funcFv));
  };
}
