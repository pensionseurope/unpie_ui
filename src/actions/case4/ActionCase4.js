import * as types from '../../constants/actionTypes';
import {fectAndDispact} from '../ActionHelper';

export function case4UpdateSlider(name, value, oldSlider) {
  const slider = Object.assign({}, oldSlider, {[name]: value});
  return {
    type: types.CASE4_UPDATE_SLIDER,
     slider
   };
}

export function case4AdjFetchDataSuccess(items) {
    return {
        type: types.CASE4_ADJINF_FETCH_DATA_SUCCESS,
        items
    };
}

export function case4NotAdjFetchDataSuccess(items) {
    return {
        type: types.CASE4_NOTADJINF_FETCH_DATA_SUCCESS,
        items
    };
}

export function case4WithoutInterestFetchDataSuccess(items) {
    return {
        type: types.CASE4_WITHOUTINTEREST_FETCH_DATA_SUCCESS,
        items
    };
}

export function case4UpdateGraph(slider) {

  const fvAdj = `https://api.unpie.eu/fv?rate=${slider.rate}&nper=${slider.nper}&pmt=-${slider.pmt}&inflation=${slider.inflation}`;
  const fvNotAdj = `https://api.unpie.eu/fv?rate=${slider.rate}&nper=${slider.nper}&pmt=-${slider.pmt}`;
  const fvSum = `https://api.unpie.eu/fv?nper=${slider.nper}&pmt=-${slider.pmt}`;

  const funcAdj = case4AdjFetchDataSuccess;
  const funcNotAdj = case4NotAdjFetchDataSuccess;
  const funcSum = case4WithoutInterestFetchDataSuccess;

    return (dispatch) => {
        dispatch(fectAndDispact(fvAdj,funcAdj));
        dispatch(fectAndDispact(fvNotAdj,funcNotAdj));
        dispatch(fectAndDispact(fvSum,funcSum));
    };
}
