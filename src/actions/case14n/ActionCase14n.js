import * as types from '../../constants/actionTypes';
import {fectAndDispact} from '../ActionHelper';

export function case14nUpdateSlider(name, value, oldSlider) {
  
  const slider = Object.assign({}, oldSlider,
     {
       [name]: value
      });
  return {
    type: types.CASE14n_UPDATE_SLIDER,
    slider
  };
}

export function fetchDataSuccess(items,slider) {
  return {
      type: types.CASE14n_FETCH_DATA_SUCCESS,
      items,
      slider
  };
}

export function case14nUpdateGraph(slider) {
  const url = `https://api.unpie.eu/wrapper.case14n?x=${slider.pensionAge}&lambda=${slider.accidentalDeathRate}&m=${slider.modalDeathRate}&b=${slider.dispersionDeathRate}&r=${slider.rate}&inflation=${slider.inflation}`;
  const func = fetchDataSuccess;
  const additionParams = [slider];
  return fectAndDispact(url,func,additionParams);
}