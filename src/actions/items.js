import * as types from '../constants/actionTypes';

export function itemsHasErrored(bool) {
    return {
        type: types.ITEMS_HAS_ERRORED,
        hasErrored: bool
    };
}

export function itemsIsLoading(bool) {
    return {
        type: types.ITEMS_IS_LOADING,
        isLoading: bool
    };
}
