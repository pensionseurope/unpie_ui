import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as actions from '../../actions/case5/ActionCase5';
import FutureValueSlider from './ValueSlider';
import {CustomizedBarChart} from '../ui/CustomizedBarChartInf';
import {generateRepeatBarData, array2BarData} from '../../utils/dataHelper';
import {sortBy} from 'lodash';
import {color} from '../../color';
import intl from 'react-intl-universal';
import { ThousandFormatter } from '../ui/Formatter';
import { styles } from '../../styles';


export const FutureValueContainer = (props) => {
  const legend = [
    {
      value: intl.get('PERIODIC_SAVINGS_PAYMENTS'), 
      type: 'square',
      color: color.primary
    },
    {
      value: intl.get('PERIODIC_SAVINGS_PAYMENTS_INCREASING_TO_ACCOUNT_FOR_INFLATION'), 
      type: 'square',
      color: color.secondary
    },
    { 
      value: intl.get('FUTURE_VALUE_NOMINAL'),  
      type: 'square',
      color: color.quaternary
    },      
    {
      value: intl.get('FUTURE_VALUE_REAL'),       
      type: 'square',
      color: color.quinary
    },
    {
      value: intl.get('SUM_OF_ALL_PERIODIC_SAVINGS_NOMINAL'),   
      type: 'square',
      color: color.tertiary
    }
  ];
  {/* Merge data into barChart data */}
  //
  const repeatData = generateRepeatBarData(props.case5.slider.nper+1, props.case5.slider.pmt,legend[0]['color']);
  const repeatData2 = array2BarData(props.case5.pmtAdjInf, legend[1]['color']);
  
  let data = [...repeatData];
  data.push(...repeatData2,...props.case5.barChart);
 data = sortBy(data, [function(x) {
    return parseInt(x.text, 10);
    }
  ]); 

  return (
    <div>
      <div className="col-sm-3">
        <FutureValueSlider
          updateSlider={props.actions.case5UpdateSlider}
          updateGraph={props.actions.case5UpdateGraph}
          slider={props.case5.slider}
          />
      </div>
      <div className="col-sm-9">
        <h3 className="text-center">
          {intl.get('CASE5_TITLE')}
        </h3>
        <CustomizedBarChart
          data={data}
          legend={legend}
          legendLayout="horizontal"
          legendVerticalAlign="bottom"
          legendAlign="center"
          />

          <div style={styles.smallMarginTop} className={"col-sm-12 col-sm-offset-1"}>
          <h4>
            {intl.get('SUM_OF_ALL_PERIODIC_SAVINGS_NOMINAL')}: <mark>{ThousandFormatter(data[data.length - 3].value)}</mark>
            <br />
            <br />
            {intl.get('FUTURE_VALUE_NOMINAL')}: <mark>{ThousandFormatter(data[data.length - 2].value)}</mark>
            <br />
            <br />
            {intl.get('FUTURE_VALUE_REAL')}: <mark>{ThousandFormatter(data[data.length - 1].value)}</mark>
          </h4>
          </div>

      </div>


    </div>
  );
};

FutureValueContainer.propTypes = {
  actions: PropTypes.object.isRequired,
  case5: PropTypes.object.isRequired
};

function mapStateToProps(state) {
  return {case5: state.case5};
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actions, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(FutureValueContainer);
