import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as actions from '../../actions/case4/ActionCase4';
import FutureValueSlider from './ValueSlider';
import {CustomizedBarChart} from '../ui/CustomizedBarChartInf';
import {generateRepeatBarData} from '../../utils/dataHelper';
import {color} from '../../color';
import intl from 'react-intl-universal';
import { ThousandFormatter } from '../ui/Formatter';
import { styles } from '../../styles';

export const FutureValueContainer = (props) => {

  const legend = [
    {
      value: intl.get('PERIODIC_SAVINGS_PAYMENT'),
      type: 'square',
      color: color.primary
    }, {
      value: intl.get('SUM_OF_ALL_PERIODIC_SAVINGS_NOMINAL'),
      type: 'square',
      color: color.secondary
    }, {
      value: intl.get('FUTURE_VALUE_NOMINAL'),
      type: 'square',
      color: color.tertiary
    }, {
      value: intl.get('FUTURE_VALUE_REAL'),
      type: 'square',
      color: color.quaternary
    }
  ];

  const data = generateRepeatBarData(
    props.case4.slider.nper + 1,
    props.case4.slider.pmt,
    legend[0].color
  );

  data.push(...props.case4.barChart);
  return (
    <div>
      <div className="col-sm-3">
        <FutureValueSlider
          updateSlider={props.actions.case4UpdateSlider}
          updateGraph={props.actions.case4UpdateGraph}
          slider={props.case4.slider}
          />
      </div>
      <div className="col-sm-9">
        <h3 className="text-center">
          {intl.get('CASE4_TITLE')}
        </h3>
        <CustomizedBarChart
          data={data}
          legend={legend}
          legendLayout="horizontal"
          legendVerticalAlign="bottom"
          legendAlign="center"
          />

          <div style={styles.smallMarginTop} className={"col-sm-12 col-sm-offset-1"}>
          <h4>
            {intl.get('SUM_OF_ALL_PERIODIC_SAVINGS_NOMINAL')}: <mark>{ThousandFormatter(data[data.length - 3].value)}</mark>
            <br />
            <br />
            {intl.get('FUTURE_VALUE_NOMINAL')}: <mark>{ThousandFormatter(data[data.length - 2].value)}</mark>
            <br />
            <br />
            {intl.get('FUTURE_VALUE_REAL')}: <mark>{ThousandFormatter(data[data.length - 1].value)}</mark>
            </h4>
          </div>

          
      </div>
    </div>
  );
};

FutureValueContainer.propTypes = {
  actions: PropTypes.object.isRequired,
  case4: PropTypes.object.isRequired
};

function mapStateToProps(state) {
  return {case4: state.case4};
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actions, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(FutureValueContainer);
