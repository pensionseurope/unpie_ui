import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as actions from '../../actions/case14n/ActionCase14n';
import FutureValueSlider from './ValueSlider';
import intl from 'react-intl-universal';
import { styles } from '../../styles';

export const FutureValueContainer = (props) => {

  return (
    <div>
      <div className="col-sm-3">
        <FutureValueSlider
          updateSlider={props.actions.case14nUpdateSlider}
          updateGraph={props.actions.case14nUpdateGraph}
          slider={props.case14n.slider} />
      </div>
      <div className="col-sm-9">
        <h3 className="text-center">
          {intl.get('CASE14n_TITLE')}
        </h3>
        <div style={styles.smallMarginTop} className={"col-sm-12 col-sm-offset-1"}>
          <h4 style={styles.mediumLineHeight}>
            {intl.get('FAIR_CONVERSION_RATE')}: <mark>{props.case14n.res[0]}</mark>
            </h4>
          </div>
      </div>
    </div>
  );
};

FutureValueContainer.propTypes = {
  actions: PropTypes.object.isRequired,
  case14n: PropTypes.object.isRequired
};

function mapStateToProps(state) {
  return {case14n: state.case14n};
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actions, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(FutureValueContainer);
