import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as actions from '../../actions/case8/ActionCase8';
import FutureValueSlider from './ValueSlider';
import {CustomizedBarChartNoLegend} from '../ui/CustomizedBarChartNoLegend';
import intl from 'react-intl-universal';
import {updateObjectInArray} from '../../reducers/ReducerHelpers';
import {color} from '../../color';
import { ThousandFormatter } from '../ui/Formatter';
import { styles } from '../../styles';

export const FutureValueContainer = (props) => {

  const item = [
    {
      index: 0,
      item: {
        "text": intl.get('YEARLY_REAL_SAVINGS_WHILE_WORKING'),
        "color": color.primary
      }
    }, {
      index: 1,
      item: {
        "text": intl.get('YEARLY_REAL_SPENDING_DURING_RETIREMENT'),
        "color": color.secondary
      }
    }
  ];

  let barchart = updateObjectInArray(props.case8.barChart, item[0]);
  barchart = updateObjectInArray(barchart, item[1]);
  return (
    <div>
      <div className="col-sm-3">
        <FutureValueSlider
          updateSlider={props.actions.case8UpdateSlider}
          updateGraph={props.actions.case8UpdateGraph}
          slider={props.case8.slider} />
      </div>
      <div className="col-sm-9">
        <h3 className="text-center">
          {intl.get('CASE8_TITLE')}
        </h3>
        <CustomizedBarChartNoLegend data={barchart}/>

        <div style={styles.smallMarginTop} className={"col-sm-12 col-sm-offset-1"}>
            <h4 style={styles.mediumLineHeight}>
            {intl.get("CASE8_RESULT_TXT")}<mark>{ThousandFormatter(props.case8.res)}</mark>
            </h4>
          </div>
      </div>
    </div>
  );
};

FutureValueContainer.propTypes = {
  actions: PropTypes.object.isRequired,
  case8: PropTypes.object.isRequired
};

function mapStateToProps(state) {
  return {case8: state.case8};
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actions, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(FutureValueContainer);
