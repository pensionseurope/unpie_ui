import React from 'react';
import PropTypes from 'prop-types';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/css/bootstrap-theme.css';
import intl from "react-intl-universal";
import http from "axios";
import {find} from 'lodash';

const SUPPOER_LOCALES = [
  {
    name: "English",
    value: "en-UK"
  },
  {
    name: "German",
    value: "de-DE"
  },
  {
    name: "French",
    value: "fr-FR"
  },
];
class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = { initDone: false };
    this.onSelectLocale = this.onSelectLocale.bind(this);
  }

  componentDidMount() {
    this.loadLocales();
  }

  // Get languageCode from URL, load from local json file,
  // then set the language using intl.init.
  loadLocales() {
    let currentLocale = intl.determineLocale({
      urlLocaleKey: "lang",
      cookieLocaleKey: "lang"
    });
    if (!find(SUPPOER_LOCALES, { value: currentLocale })) {
      currentLocale = "en-UK";
    }

    http
      .get(`locales/${currentLocale}.json`)
      .then(res => {
        //console.log("App locale data", res.data);
        return intl.init({
          currentLocale,
          locales: {
            [currentLocale]: res.data
          }
        });
      })
      .then(() => {
        this.setState({ initDone: true });
      });
  }

  renderLocaleSelector() {
    return (
      <select onChange={this.onSelectLocale} defaultValue="">
        <option value="" disabled>Change Language</option>
        {SUPPOER_LOCALES.map(locale => (
          <option key={locale.value} value={locale.value}>{locale.name}</option>
        ))}
      </select>
    );
  }

  onSelectLocale(e) {
    let lang = e.target.value;
    location.search = `?lang=${lang}`;
  }

  render() {
    return (
      this.state.initDone &&
      <div>

        <div style={style} className="container">
          {this.props.children}
        </div>
      </div>
    );
  }
}

const style = {};

App.propTypes = {
  children: PropTypes.element,
  location: PropTypes.object
};

export default App;
