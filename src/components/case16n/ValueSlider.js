import React from 'react';
import PropTypes from 'prop-types';
import CustomizedDoubleSlider from '../ui/CustomizedDoubleSlider';
import CustomizedDoubleSliderSmallDigits from '../ui/CustomizedDoubleSliderSmallDigits';
import CustomizedSlider from '../ui/CustomizedSliderCase16n';
import intl from 'react-intl-universal';
import {styles} from '../../styles';

class ValueSlider extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.handleChange = this.handleChange.bind(this);
    this.handleUpdate = this.handleUpdate.bind(this);
  }

  handleUpdate(){
    this.props.updateGraph(this.props.slider);
  }

  handleChange(name, event, value) {
    this.props.updateSlider(name,value,this.props.slider);
  }

  render() {
    const {slider,updateGraph, updateSlider} = this.props;
    return (
      <div style={styles.bigMarginTop}>
        <div style={styles.sliderBox2}>
          <CustomizedDoubleSlider
            updateGraph = {updateGraph}
            updateSlider= {updateSlider}
            slider = {slider}
            title = {intl.get('REMAINING_LIFETIME2')}
            name1 = "remainingLifetime1"
            name2 = "remainingLifetime2"
            min = {1}
            max = {50}
            step = {1}
            value1 = {slider.remainingLifetime1}
            value2 = {slider.remainingLifetime2}
          />
        </div>
      <div style={styles.sliderBox2}>
          <CustomizedSlider
            updateGraph = {updateGraph}
            updateSlider= {updateSlider}
            slider = {slider}
            title = {intl.get('PENSION_AGE')}
            name = "pensionAge1"
            min = {30}
            max = {100}
            step = {1}
            value = {slider.pensionAge1}
          />
          <CustomizedDoubleSliderSmallDigits
            updateGraph = {updateGraph}
            updateSlider= {updateSlider}
            slider = {slider}
            title = {intl.get('ACCIDENTAL_DEATH_RATE')}
            name1 = "accidentalDeathRate1"
            name2 = "accidentalDeathRate2"
            min = {0}
            max = {0.02}
            step = {0.001}
            value1 = {slider.accidentalDeathRate1}
            value2 = {slider.accidentalDeathRate2}
          />
          <CustomizedDoubleSlider
            updateGraph = {updateGraph}
            updateSlider= {updateSlider}
            slider = {slider}
            title = {intl.get('MODAL_DEATH_RATE')}
            name1 = "modalDeathRate1"
            name2 = "modalDeathRate2"
            min = {70}
            max = {90}
            step = {0.1}
            value1 = {slider.modalDeathRate1}
            value2 = {slider.modalDeathRate2}
          />
          <CustomizedDoubleSlider
            updateGraph = {updateGraph}
            updateSlider= {updateSlider}
            slider = {slider}
            title = {intl.get('DISPERSION_DEATH_RATE')}
            name1 = "dispersionDeathRate1"
            name2 = "dispersionDeathRate2"
            min = {1}
            max = {20}
            step = {0.1}
            value1 = {slider.dispersionDeathRate1}
            value2 = {slider.dispersionDeathRate2}
          />
        </div>
      </div>
    );
  }
}
ValueSlider.propTypes = {
  updateSlider: PropTypes.func.isRequired,
  updateGraph: PropTypes.func.isRequired,
  slider: PropTypes.object.isRequired,
};

export default ValueSlider;




