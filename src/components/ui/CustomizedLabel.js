import React from 'react';
import PropTypes from 'prop-types';
import {round} from 'lodash';
import {ThousandFormatter} from './Formatter';

export function CustomizedLabel(props) {
  const {x, y, fill, value, width} = props;
  return (
    <text
      x={x}
      y={y}
      dy={-4}
      dx={width/2}
      fontSize={16}
      fontFamily={'sans-serif'}
      fill={fill}
      textAnchor="middle">{ThousandFormatter(round(value,2))}</text>);
}

CustomizedLabel.propTypes = {
  x: PropTypes.number,
  y: PropTypes.number,
  fill: PropTypes.string,
  value: PropTypes.number,
  width: PropTypes.number,
};
