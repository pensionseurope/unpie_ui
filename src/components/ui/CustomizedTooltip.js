import React from 'react';
import PropTypes from 'prop-types';
import intl from 'react-intl-universal';
import {ThousandFormatter} from './Formatter';


export const CustomizedTooltip = (props) => {
    const { active, payload, external, label,text } = props;
  
    if (active) {
      const style = {
        padding: 6,
        backgroundColor: '#fff',
        border: '1px solid #ccc',
      };
  
      const currData = external.filter(entry => (entry.text === label))[0];
      return (
        <div className="chart-tooltip" style={style}>
          <p>{text} = {currData.text}</p>
          <p>{intl.get('VALUE') + '='}{ThousandFormatter(payload[0].value,2)}</p>
        </div>
      );
    }
    return null;
};

CustomizedTooltip.propTypes = {
  active: PropTypes.bool,
  payload: PropTypes.array,
  external: PropTypes.array,
  label: PropTypes.string,
  text: PropTypes.string
};

CustomizedTooltip.defaultProps = {
  text: "T"
};
