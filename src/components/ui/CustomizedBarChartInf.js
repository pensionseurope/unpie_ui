import React from 'react';
import PropTypes from 'prop-types';
import {
  XAxis,
  YAxis,
  CartesianGrid,
  Bar,
  BarChart,
  Cell,
  Legend,
  Tooltip,
  ResponsiveContainer,
  Label
} from 'recharts';

import {CustomizedTooltip} from './CustomizedTooltip';
import {ThousandFormatter} from './Formatter';


export function CustomizedBarChart(props) {
  const {data,legend,legendLayout,legendAlign,legendVerticalAlign,xAxisLabel} = props;
  return (
    <ResponsiveContainer width={'100%'} height={360}>
    <BarChart data={data}
      margin={{
      top: 60,
      right: 20,
      left: 20,
      bottom: 2
    }}>
      <XAxis dataKey="text" fontFamily="sans-serif" dy={1} type={'category'} >
        <Label value={xAxisLabel} offset={-5} dx={15} position="insideTopRight" />
      </XAxis>
      <YAxis scale="sqrt" tickFormatter={ThousandFormatter} />
      <Tooltip content={<CustomizedTooltip external={data} text={xAxisLabel} />} />
      <CartesianGrid vertical={false} stroke="#ebf3f0"/>
        <Legend payload={legend} layout={legendLayout} align={legendAlign} verticalAlign={legendVerticalAlign} wrapperStyle={style} />
      <Bar dataKey="value" barSize ={170}>
        {data.map((entry, index) => (<Cell key={index} fill={entry.color}/>))}
      </Bar>
    </BarChart>
    </ResponsiveContainer>
  );
}

const style = {
      paddingTop: '10px',
      lineHeight: '24px',
      left: '40px'
    };


CustomizedBarChart.propTypes = {
  data: PropTypes.array.isRequired,
  legend: PropTypes.array,
  legendLayout: PropTypes.string,
  legendAlign: PropTypes.string,
  legendVerticalAlign: PropTypes.string,
  xAxisLabel: PropTypes.string,
};

CustomizedBarChart.defaultProps = {
  legendLayout: "vertical",
  legendAlign: "right",
  legendVerticalAlign: "middle",
  xAxisLabel: "T"
};
