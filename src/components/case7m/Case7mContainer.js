import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as actions from '../../actions/case7m/ActionCase7m';
import FutureValueSlider from './ValueSlider';
import {CustomizedBarChart} from '../ui/CustomizedBarChartGroupTick';
import {color} from '../../color';
import intl from 'react-intl-universal';
import { styles } from '../../styles';

export const FutureValueContainer = (props) => {
  
  const legend = [
    {
      value: intl.get('REQUIRED_WEALTH_TO_FINANCE_CONSTANT_NOMINAL_PERIODIC_SPENDING'), 
      type: 'square',
      color: color.primary
    }, {
      value: intl.get('REQUIRED_WEALTH_TO_FINANCE_CONSTANT_REAL_PERIODIC_SPENDING'), 
      type: 'square',
      color: color.secondary
    }, {
      value: intl.get('REQUIRED_WEALTH_TO_FINANCE_CONSTANT_NOMINAL_PERIODIC_SPENDING_REFERENCE'), 
      type: 'square',
      color: color.tertiary
    //}, {
    //  value: intl.get('REQUIRED_WEALTH_TO_FINANCE_CONSTANT_REAL_PERIODIC_SPENDING_REFERENCE'), 
    //  type: 'square',
    //  color: color.quaternary
    }
  ];

  let barchart =props.case7m.barChart;
 /* barchart[0].color = legend[0].color;
  barchart[1].color = legend[1].color;
  barchart[2].color = legend[2].color;
  barchart[3].color = legend[3].color;
*/
  return (
    <div>
      <div className="col-sm-3">
        <FutureValueSlider
          updateSlider={props.actions.case7mUpdateSlider}
          updateGraph={props.actions.case7mUpdateGraph}
          slider={props.case7m.slider}/>
      </div>
      <div className="col-sm-9">
        <h3 className="text-center">
          {intl.get('CASE7m_TITLE')}
        </h3>
        <CustomizedBarChart
          data={barchart}
          legend={legend}
          legendLayout="horizontal"
          legendVerticalAlign="bottom"
          legendAlign="left"/>

          <div style={styles.smallMarginTop} className={"col-sm-12 col-sm-offset-1"}>
          <h4>
            {intl.get('EXPECTED_REMAINING_LIFETIME')}: <mark>{props.case7m.res[0]}</mark>
            <br />
            <br />
            {intl.get('FAIR_CONVERSION_RATE')}: <mark>{props.case7m.res[1]}</mark>
            </h4>
          </div>
      </div>
    </div>
  );
};

FutureValueContainer.propTypes = {
  actions: PropTypes.object.isRequired,
  case7m: PropTypes.object.isRequired
};

function mapStateToProps(state) {
  return {case7m: state.case7m};
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actions, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(FutureValueContainer);
