import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as actions from '../../actions/case15n/ActionCase15n';
import FutureValueSlider from './ValueSlider';
import intl from 'react-intl-universal';
import { styles } from '../../styles';

export const FutureValueContainer = (props) => {

  return (
    <div>
      <div className="col-sm-3">
        <FutureValueSlider
          updateSlider={props.actions.case15nUpdateSlider}
          updateGraph={props.actions.case15nUpdateGraph}
          slider={props.case15n.slider} />
      </div>
      <div className="col-sm-9">
        <h3 className="text-center">
          {intl.get('CASE15n_TITLE')}
        </h3>
        <div style={styles.smallMarginTop} className={"col-sm-12 col-sm-offset-1"}>
          <h4 style={styles.mediumLineHeight}>
            {intl.get('PROBABILITY_OF_RETIREMENT_RUIN')}: <mark>{props.case15n.res[1]}</mark>
            <br />
            <br />
            {intl.get('MORTALITY_RATE')}: <mark>{props.case15n.res[0]}</mark>
            </h4>
          </div>
      </div>
    </div>
  );
};

FutureValueContainer.propTypes = {
  actions: PropTypes.object.isRequired,
  case15n: PropTypes.object.isRequired
};

function mapStateToProps(state) {
  return {case15n: state.case15n};
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actions, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(FutureValueContainer);
