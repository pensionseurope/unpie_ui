import React from 'react';
import PropTypes from 'prop-types';
import CustomizedSlider from '../ui/CustomizedSliderNoUpdate';
import intl from 'react-intl-universal';
import {styles} from '../../styles';

class ValueSlider extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(name, event, value) {
    this.props.updateSlider(name,value,this.props.slider);
  }

  render() {
    const {slider,updateSlider} = this.props;
    return (
      <div style={styles.bigMarginTop}>
        <div style={styles.sliderBox}>
            <CustomizedSlider
            updateSlider= {updateSlider}
            slider = {slider}
            title = {intl.get('WEALTH_AT_RETIREMENT')}
            name = "wealth"
            min = {0}
            max = {1000000}
            step = {1000}
            value = {slider.wealth}
          />

          <CustomizedSlider
            updateSlider= {updateSlider}
            slider = {slider}
            title = {intl.get('MINIMUM_TIME_TO_RUIN')}
            name = "mtr"
            min = {1}
            max = {25}
            step = {1}
            value = {slider.mtr}
          />

          <CustomizedSlider
            updateSlider= {updateSlider}
            slider = {slider}
            title = {intl.get('PROBABILITY_TO_EXCEED_MINIMUM_TIME_TO_RUIN')}
            name = "prob"
            min = {0}
            max = {1}
            step = {0.005}
            value = {slider.prob}
          />
        </div>

        <div style={styles.sliderBox}>
          <CustomizedSlider
            updateSlider= {updateSlider}
            slider = {slider}
            title = {intl.get('EXPECTED_REAL_RETURN')}
            name = "mu"
            min = {0}
            max = {0.25}
            step = {0.005}
            value = {slider.mu}
          />
        
          <CustomizedSlider
            updateSlider= {updateSlider}
            slider = {slider}
            title = {intl.get('VOLATILITY_OF_EXPECTED_REAL_RETURN')}
            name = "vol"
            min = {0}
            max = {0.25}
            step = {0.005}
            value = {slider.vol}
          />

          </div>
        </div>

    );
  }
}



ValueSlider.propTypes = {
  updateSlider: PropTypes.func.isRequired,
  slider: PropTypes.object.isRequired,
};

export default ValueSlider;
