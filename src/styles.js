{/*Styles*/}
import {color} from './color';

export const styles = {
  sliderBox : {
     borderStyle:"solid",
     borderColor:color.secondary,
     padding: "20px",
     paddingBottom: "0px",
     marginBottom:"5px"
  },
  sliderBox2 : {
    borderStyle:"solid",
    borderColor:color.secondary,
    padding: "20px",
    paddingBottom: "100px",
    marginBottom:"5px"
 },
  bigMarginTop:{
      marginTop:"100px"
  },
  smallMarginTop:{
      marginTop:"30px"
  },
  sliderBoxBtn : {
    borderStyle:"solid",
    borderColor:color.secondary,
    paddingBottom: "0px",
    marginBottom:"5px"
 },
  button:{
    boxSizing: "border-box",
    display: "block",
    width: "100%",
    backgroundColor:color.secondary,
    color: "White",
    borderStyle: "none",
    fontSize:"20px",
    ':hover': {
        backgroundColor: color.primary
      }
  },
  mediumLineHeight:{
      lineHeight:"25px"
  },
  table:{
      border: "1px solid black",
  },
  th:{
      padding: "5px",
      textAlign: "left"
  },
  td:{
    borderStyle:"solid",
    borderColor:color.secondary,
    padding: "10px",
    textAlign: "left",
    fontWeight: "bold"
   },
   bigPaddingTop:{
       paddingTop:"30px"
   },
  
   table2 : {
    margin: "15px auto",
    borderCollapse: "collapse",
    border: "1px solid #eee",
    tr :{
       ':hover' : {
        background: "#f4f4f4",        
        td:{
          color:" #555"
        }
      }
    },
    td: {
      color: "#999",
      border: "1px solid #eee",
      padding: "6px",
      borderCollapse: "collapse"
    },
    th:{
      background: color.secondary,
      color: "#fff",
      fontSize: "14px",
      padding: "8px",
      borderBottom: "1px solid #eee",
      '.last': {
        borderRight: "none"
      }
    }
  },

  table3 : {
    margin: "15px auto",
    borderCollapse: "collapse",
    border: "1px solid #eee",
    tr :{
       ':hover' : {
        background: "#f4f4f4",        
        td:{
          color:" #555"
        }
      }
    },
    td: {
      color: "#999",
      border: "1px solid #eee",
      padding: "6px",
      borderCollapse: "collapse"
    },
    th1:{
      background: color.primary,
      color: "#fff",
      fontSize: "14px",
      padding: "8px",
      borderBottom: "1px solid #eee",
      '.last': {
        borderRight: "none"
      }
    },
    th2:{
      background: color.tertiary,
      color: "#fff",
      fontSize: "14px",
      padding: "8px",
      borderBottom: "1px solid #eee",
      '.last': {
        borderRight: "none"
      }
    }
  }
};
