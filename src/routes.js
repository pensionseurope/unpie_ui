import React from 'react';
import { Route, IndexRoute } from 'react-router';

import App from './components/App';

import case1 from './components/case1/Case1Container';
import case2 from './components/case2/Case2Container';
import case3 from './components/case3/Case3Container';
import case4 from './components/case4/Case4Container';
import case5 from './components/case5/Case5Container';
import case6 from './components/case6/Case6Container';
import case7 from './components/case7/Case7Container';
import case7m from './components/case7m/Case7mContainer';
import case8 from './components/case8/Case8Container';
import case8m from './components/case8m/Case8mContainer';
import case9 from './components/case9/Case9Container';
import case9m from './components/case9m/Case9mContainer';
import add1 from './components/add1/add1Container';
import add2 from './components/add2/add2Container';
import add3 from './components/add3/add3Container';
import add4 from './components/add4/add4Container';
import case14n from './components/case14n/Case14nContainer';
import case15n from './components/case15n/Case15nContainer';
import case16n from './components/case16n/Case16nContainer';
import AboutPage from './components/AboutPage';
import NotFoundPage from './components/NotFoundPage';

export default (
  <Route path="/" component={App}>
    <IndexRoute component={case1}/>
    <Route path="case1" component={case1}/>
    <Route path="case2" component={case2}/>
    <Route path="case3" component={case3}/>
    <Route path="case4" component={case4}/>
    <Route path="case5" component={case5}/>
    <Route path="case6" component={case6}/>
    <Route path="case7" component={case7}/>
    <Route path="case7m" component={case7m}/>
    <Route path="case8" component={case8}/>
    <Route path="case8m" component={case8m}/>
    <Route path="case9" component={case9}/>
    <Route path="case9m" component={case9m}/>
    <Route path="add1" component={add1}/>
    <Route path="add2" component={add2}/>
    <Route path="add3" component={add3}/>
    <Route path="add4" component={add4}/>
    <Route path="case14n" component={case14n}/>
    <Route path="case15n" component={case15n}/>
    <Route path="case16n" component={case16n}/>
    <Route path="about" component={AboutPage}/>
    <Route path="*" component={NotFoundPage}/>
  </Route>
);
