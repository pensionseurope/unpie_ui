import {combineReducers} from 'redux';
import {routerReducer} from 'react-router-redux';
import {itemsHasErrored,itemsIsLoading} from './GeneralReducer';
import {case1} from './Case1Reducer';
import {case2} from './Case2Reducer';
import {case3} from './Case3Reducer';
import {case4} from './Case4Reducer';
import {case5} from './Case5Reducer';
import {case6} from './Case6Reducer';
import {case7} from './Case7Reducer';
import {case7m} from './Case7mReducer';
import {case8} from './Case8Reducer';
import {case8m} from './Case8mReducer';
import {case9} from './Case9Reducer';
import {case9m} from './Case9mReducer';
import {add1} from './Add1Reducer';
import {add2} from './Add2Reducer';
import {add3} from './Add3Reducer';
import {add4} from './Add4Reducer';
import {case14n} from './Case14nReducer';
import {case15n} from './Case15nReducer';
import {case16n} from './Case16nReducer';


const rootReducer = combineReducers({
  case1,
  case2,
  case3,
  case4,
  case5,
  case6,
  case7,
  case7m,
  case8,
  case8m,
  case9,
  case9m,
  add1,
  add2,
  add3,
  add4,
  case14n,
  case15n,
  case16n,
  itemsHasErrored,
  itemsIsLoading,
  routing: routerReducer
});

export default rootReducer;
