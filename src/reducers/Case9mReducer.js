import * as types from '../constants/actionTypes';
import initialState from './initialState';
import {updateObjectInArray} from './ReducerHelpers';
import {color} from '../color';

export function case9m(state = initialState.case9m, action) {
  switch (action.type) {
    case types.CASE9m_UPDATE_SLIDER:
    {
      return {
        ...state,
        slider: action.slider
      };
    }

    case types.CASE9m_FETCH_DATA_SUCCESS:
    {
      const item = [
        {
          index: 0,
          item: {
            "value": action.items.constant_real_yearly_savings_payment[0],          },
          color: color.primary
        }, {
          index: 1,
          item: {
            "value": action.slider.pmt,
          },
          color: color.secondary
        }
      ];

      let barChart = updateObjectInArray(state.barChart,item[0]);
      barChart = updateObjectInArray(barChart,item[1]);

      const res = [action.items.expected_remaining_lifetime,action.items.fair_conversion_rate,action.items.requiredWealth];

      return {
        ...state,
        barChart: barChart,
        res: res,
        slider: action.slider
      };
    }
  default:
    return state;
  }
}
