import * as types from '../constants/actionTypes';
import initialState from './initialState';
import {updateObjectInArray} from './ReducerHelpers';

export function case7(state = initialState.case7, action) {
  switch (action.type) {
    case types.CASE7_UPDATE_SLIDER:
    {
      return {
        ...state,
        slider: action.slider
      };
    }

    case types.CASE7_SPENDING_REQUIRED_ADJINFLATION_FETCH_DATA_SUCCESS:
      {
        const item = {
          index: 1,
          item: {
            "value": action.items[action.items.length-1]
          }
        };
        const barChart = updateObjectInArray(state.barChart, item);
        return {
          ...state,
          barChart
        };
      }

    case types.CASE7_SPENDING_REQUIRED_NOTADJINFLATION_FETCH_DATA_SUCCESS:
      {
        const item = {
          index: 0,
          item: {
            "value": action.items[action.items.length-1]
          }
        };
        const barChart = updateObjectInArray(state.barChart, item);
        return {
          ...state,
          barChart
        };
      }

    case types.CASE7_SPENDING_ADJINFLATION_FETCH_DATA_SUCCESS:{
        return {
          ...state,
          spendingAdjInf:action.items
        };
    }

    default:
      return state;
  }
}
