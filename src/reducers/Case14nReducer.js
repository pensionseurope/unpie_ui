import * as types from '../constants/actionTypes';
import initialState from './initialState';

export function case14n(state = initialState.case14n, action) {
  switch (action.type) {
    case types.CASE14n_UPDATE_SLIDER:
    {
      return {
        ...state,
        slider: action.slider
      };
    }

    case types.CASE14n_FETCH_DATA_SUCCESS:
    {
        const res = [action.items.fair_conversion_rate];

      return {
        ...state,
        res: res,
        slider: action.slider
      };
    }
  default:
    return state;
  }
}
