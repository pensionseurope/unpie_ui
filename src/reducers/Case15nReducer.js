import * as types from '../constants/actionTypes';
import initialState from './initialState';

export function case15n(state = initialState.case15n, action) {
  switch (action.type) {
    case types.CASE15n_UPDATE_SLIDER:
    {
      return {
        ...state,
        slider: action.slider
      };
    }

    case types.CASE15n_FETCH_DATA_SUCCESS:
    {
        const res = [action.items.mortality_rate,action.items.probability_of_retirement_ruin_when_starting_at_wealth_w];

      return {
        ...state,
        res: res,
        slider: action.slider
      };
    }
  default:
    return state;
  }
}