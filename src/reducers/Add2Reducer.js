import * as types from '../constants/actionTypes';
import initialState from './initialState';
import {mapArrayObjAdd2} from './ReducerHelpers';

export function add2(state = initialState.add2, action) {
  switch (action.type) {
    case types.ADD2_UPDATE_SLIDER:
      {
        return {
          ...state,
          slider: action.slider
        };
      }
    case types.ADD2_FETCH_DATA_SUCCESS:
      {
        const LifeLongSorted = action.items.Lifelong_pensions_sorted;
        const FutureValueSorted = action.items.Future_value_sorted;
        const Scenarios = mapArrayObjAdd2(action.items.depot_scenariros);
        const root = action.items.perodic_savings;
        return {
          ...state,
          LifeLongSorted,
          Scenarios,
          root,
          FutureValueSorted,
          slider: action.slider,
          result: action.items[0]
        };
      }
    default:
      return state;
  }
}
