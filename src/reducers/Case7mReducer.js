import * as types from '../constants/actionTypes';
import initialState from './initialState';
import {updateObjectInArray} from './ReducerHelpers';
import {color} from '../color';

export function case7m(state = initialState.case7m, action) {
  switch (action.type) {
    case types.CASE7m_UPDATE_SLIDER:
    {
      return {
        ...state,
        slider: action.slider
      };
    }

    case types.CASE7m_FETCH_DATA_SUCCESS:
    {
      const item = [
        {
          index: 0,
          item: {
            "value": action.items.required_wealth_to_finance_constant_nominal_periodic_spending[0],
          },
          color: color.primary
        }, {
          index: 1,
          item: {
            "value": action.items.required_wealth_to_finance_constant_real_periodic_spending[0],
          },
          color: color.secondary
        }, {
          index: 2,
          item: {
            "value": action.items.required_wealth_to_finance_constant_nominal_periodic_spending_reference[0],
          },
          color: color.tertiary
        //}, {
        //  index: 3,
        //  item: {
        //    "value": action.items.required_wealth_to_finance_constant_real_periodic_spending_reference[0],
        //  },
        //  color: color.quaternary
        }
      ];

      let barChart = updateObjectInArray(state.barChart,item[0]);
      barChart = updateObjectInArray(barChart,item[1]);
      barChart = updateObjectInArray(barChart,item[2]);
      //barChart = updateObjectInArray(barChart,item[3]);

      const res = [action.items.expected_remaining_lifetime,action.items.fair_conversion_rate];

      return {
        ...state,
        barChart: barChart,
        res: res,
        slider: action.slider
      };
    }
  default:
    return state;
  }
}
