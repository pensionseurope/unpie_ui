{/*Color Scheme*/}

export const color = {
  primary: '#FFA500', //Orange
  secondary: '#808080', //Grey
  tertiary: '#2E8B57', //Green
  quaternary: '#1976D2', //Blue
  quinary: '#030301', //Black
  senary: '#A59276',
  septenary: '#666666',
  octonary: '#8E1E9C',
  nonary: '#AA4400',
  denary: '#846E92'
};
